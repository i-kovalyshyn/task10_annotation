package edu.kovalyshyn.model;

import java.util.Arrays;

public class SomeClass {
    public void myMethod(String a, int... args) {
        System.out.println(a);
        System.out.println(Arrays.toString(args));
    }

    public void myMethod(String... args) {
        System.out.println(Arrays.toString(args));
    }
}
