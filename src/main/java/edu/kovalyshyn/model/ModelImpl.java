package edu.kovalyshyn.model;

import edu.kovalyshyn.reflection.SomeClassReflection;
import edu.kovalyshyn.reflection.UserReflection;
import edu.kovalyshyn.view.MyView;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ModelImpl implements Model {
    private static Logger log = LogManager.getLogger(ModelImpl.class);

    @Override
    public void task1() {
        new UserReflection().printAnnotatedFields();
    }

    @Override
    public void task2() {
        new SomeClassReflection().invokeMyMethod(new SomeClass());
    }

    @Override
    public void task3() {
        System.out.println("Enter something");
        String value = MyView.input.next();
        new UserReflection().setSomeValue(value);
    }


    @Override
    public void task4() {
new UserReflection().refUnknownObject(new User());
    }
}
