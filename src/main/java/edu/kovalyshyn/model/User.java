package edu.kovalyshyn.model;

import lombok.*;

@NoArgsConstructor
@Data
@ToString

public class User {
    @MyAnnotation(myName = "some Name")
    private String myName;
    @MyAnnotation
    private int age;
private void getInfo(){
    System.out.println("from getInfo()");
}

}
