package edu.kovalyshyn.reflection;

import edu.kovalyshyn.model.SomeClass;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class SomeClassReflection {
    public void invokeMyMethod(SomeClass someClass){
        Class<SomeClass> c = SomeClass.class;

        try{
            Method method1 = c.getDeclaredMethod("myMethod", String.class, int[].class);
            Method method2 = c.getDeclaredMethod("myMethod", String[].class);
            method1.invoke(someClass,"from some Class", new int[]{1,2,3});
            method2.invoke(someClass,new Object[]{new String[]{"one","two","three"}});
        }catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e){
            e.printStackTrace();
        }
    }
}
