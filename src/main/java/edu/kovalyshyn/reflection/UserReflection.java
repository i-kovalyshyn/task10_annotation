package edu.kovalyshyn.reflection;

import edu.kovalyshyn.model.MyAnnotation;
import edu.kovalyshyn.model.User;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class UserReflection {
    public  void printAnnotatedFields() {
        Class<?> clazz = User.class;
        Field[] fields = clazz.getDeclaredFields();
        for (Field f : fields) {
            if (f.isAnnotationPresent(MyAnnotation.class)) {
                f.setAccessible(true);
                MyAnnotation myAnnotation = (MyAnnotation) f.getAnnotation(MyAnnotation.class);
                String myName = myAnnotation.myName();
                int age = myAnnotation.age();
                System.out.println("field: " + f.getName());
                System.out.println(" name in @: " + myName);
                System.out.println(" age in @: "+ age);
            }
        }

    }
    public void setSomeValue(String value){
        User user = new User();
        Class<?> clazz = user.getClass();
        for (Field field: clazz.getDeclaredFields()){
            field.setAccessible(true);
            if (field.getType() == String.class){
                System.out.println(field.getName());
                try{
                    field.set(user, value);
                    System.out.println(field.get(user));
                }catch (IllegalAccessException e){
                    System.out.println(e.getMessage());
                }
            }
        }
    }

    public void refUnknownObject(Object object){
        try {
            Class<?> clazz = object.getClass();
            System.out.println("The name of class is: " + clazz.getSimpleName());
            System.out.println("The name of class is: " + clazz.getName());

            Constructor<?> constructor = clazz.getConstructor();
            System.out.println("The name of constructor is : "+constructor.getName());

            System.out.println("The declared methods of class are" );
            Method[] methods = clazz.getMethods();
            for (Method method:methods){
                System.out.println("  "+method.getName());
            }
            System.out.println();
            System.out.println("The declared fields of class are : ");
            Field[] fields = clazz.getDeclaredFields();
            for (Field field:fields){
                System.out.println("  "+ field.getName());
            }
            Method method = clazz.getDeclaredMethod("getInfo");
            method.setAccessible(true);
            method.invoke(object);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }
}
