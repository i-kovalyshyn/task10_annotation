package edu.kovalyshyn.controller;

import edu.kovalyshyn.model.Model;
import edu.kovalyshyn.model.ModelImpl;

public class ControllerImpl implements Controller {
    private Model model;

    public ControllerImpl() {
        model = new ModelImpl();
    }

    @Override
    public void task1() {
        model.task1();
    }

    @Override
    public void task2() {
        model.task2();
    }

    @Override
    public void task3() {
        model.task3();
    }

    @Override
    public void task4() {
        model.task4();
    }
}
